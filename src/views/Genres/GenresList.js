import React, { Component } from "react";
import GenreService from "../../services/genre.service";
import GenresGallery from "../../components/Gallery/GenresGallery";

export default class GenresList extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
      genres: []
    };
  }
  
  async componentDidMount() {
    try {
      let response = await GenreService.list();
      this.setState({ genres: response.data.genres })
    } catch (e) {
      console.error(e);
    }
  }
  

  render() {
    let { genres } = this.state;

    return <div>
      <h1>Genres</h1>
      <GenresGallery genres={ genres } />
    </div>
  }

}
