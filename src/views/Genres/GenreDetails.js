import React, { Component } from "react";
import GenreService from "../../services/genre.service";
import Song from "../../components/Song";
import { Button, Col, Image, Row, Table } from "react-bootstrap";
import { connect } from "react-redux";
import { updatePlayerList } from "../../actions/player.action";

class GenreDetails extends Component {

  constructor(props) {
    super(props);

    this.state = {
      genre: null,
      songs:null
    };
  }
  async componentDidMount() {
    const { match: { params } } = this.props;
    try {
      let response = await GenreService.details(params.id);
      this.setState({
        genre: response.data.genre,
        songs: response.data.songs
      });
    } catch (e) {
      console.error(e);
    }
  }

  playGenres() {
    this.props.updatePlayerList(this.state.songs);
  }

  render() {
    let { genre, songs } = this.state;
    return genre && songs && <div>
      <Row className="mb-3">
        <Col md="auto">
          <Image src="../no_cover.jpg" roundedCircle fluid />
        </Col>
        <Col>
          <h1>{ genre.libelle }</h1>
          {
            songs.length > 0 && <Button variant="light" className="" onClick={ () => this.playGenres() }>
              <i className="fas fa-play mr-2 text-success"></i>Lire la playlist
            </Button>
          }
        </Col>
      </Row>

      <Table>
        <thead>
        <tr>
          <td></td>
          <td>Titre</td>
          <td>Artistes</td>
          <td>Année</td>
        </tr>
        </thead>
        <tbody>
        {
          songs && songs.map((song, index) => {
            return <Song
              key={ index }
              id={ song._id }
              name={ song.title }
              artists={ song.artists }
              year={ song.year } />
          })
        }
        </tbody>
      </Table>
    </div>
  }

}

const mapStateToProps = state => {
  return {
    playerList : state.playerList,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updatePlayerList: playerList => dispatch(updatePlayerList(playerList)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GenreDetails);
