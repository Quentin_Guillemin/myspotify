import React, { Component } from "react";
import { connect } from "react-redux";
import { updateUser, updateToken } from "../actions/user.action";
import UserService from "../services/user.service";
import { Button, Card, Form } from "react-bootstrap";

class Login extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
      name: "",
      email: "",
      password: "",
      isErrorLogin: false,
      isErrorRegister: false
    };
  }
  
  handleChange(e) {
    this.setState({ [e.target.id]: e.target.value });
  }

  async login(e) {
    e.preventDefault();
    this.setState({ isErrorLogin: false });
    let { email, password } = this.state;
    let body = { email, password };
    try {
      let response = await UserService.auth(body);
      this.props.updateUser(response.data.user);
      this.props.updateToken(response.data.token);
    } catch (e) {
      this.setState({ isErrorLogin: true });
    }
  }

  async register(e) {
    e.preventDefault();
    this.setState({ isErrorRegister: false });
    let { name, email, password } = this.state;
    let body = { name, email, password };
    try {
      let response = await UserService.create(body);
      this.props.updateUser(response.data.user);
      this.props.updateToken(response.data.token);
    } catch (e) {
      this.setState({ isErrorRegister: true });
    }
  }

  render() {
    let { isErrorLogin, isErrorRegister } = this.state;

    return <div className="d-flex justify-content-center">
      <div>
        <h1 className="text-center font-weight-bold">My Spotify</h1>
        <div className="py-4">
          <Card bg="dark" style={{ width: '18rem' }}>
            <Card.Body>
              <Card.Title className="text-center">Connexion</Card.Title>
              <Form onSubmit={ (e) => this.login(e) }>

                <div className="form-group">
                  <label>Email</label>
                  <input type="email" required className="form-control" id="email"
                    onChange={ (e) => this.handleChange(e) }/>
                </div>

                <div className="form-group">
                  <label>Mot de passe</label>
                  <input type="password" required className="form-control" id="password"
                    onChange={ (e) => this.handleChange(e) }/>
                </div>

              <Button type="submit" className="primary w-100">Connexion</Button>

              { isErrorLogin && <div className="pt-3">Erreur d'email et / ou de mot de passe</div> }

            </Form>
            </Card.Body>
          </Card>
        </div>
        <div className="py-4">
          <Card bg="dark" style={{ width: '18rem' }}>
            <Card.Body>
              <Card.Title className="text-center">Inscription</Card.Title>
              <Form onSubmit={ (e) => this.register(e) }>

              <div className="form-group">
                  <label>Nom</label>
                  <input type="text" required className="form-control" id="name"
                    onChange={ (e) => this.handleChange(e) }/>
                </div>

                <div className="form-group">
                  <label>Email</label>
                  <input type="email" required className="form-control" id="email"
                    onChange={ (e) => this.handleChange(e) }/>
                </div>

                <div className="form-group">
                  <label>Mot de passe</label>
                  <input type="password" required className="form-control" id="password"
                    onChange={ (e) => this.handleChange(e) }/>
                </div>

              <Button type="submit" className="primary w-100">Inscription</Button>

              { isErrorRegister && <div className="pt-3">Impossible de créer le compte</div> }

            </Form>
            </Card.Body>
          </Card>
        </div>
      </div>
    </div>
  }

}

const mapStateToProps = state => {
  return {
    user: state.user,
    userToken: state.userToken
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateUser: user => dispatch(updateUser(user)),
    updateToken: token => dispatch(updateToken(token))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

