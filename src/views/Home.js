import React, { Component } from "react";
import UserService from "../services/user.service";
import ArtistsGallery from "../components/Gallery/ArtistsGallery";
import AlbumsGallery from "../components/Gallery/AlbumsGallery";
import PlaylistsGallery from "../components/Gallery/PlaylistsGallery";

export default class Home extends Component {

  constructor(props) {
    super(props);

    this.state = {
      user: null
    };
  }

  async componentDidMount() {
    try {
      let response = await UserService.details();
      this.setState({ user: response.data.user });
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    let { user } = this.state;

    return user && <div>
      <h1>Bibliotèque de { user.name }</h1>

      <div className="">
        <h2>Mes Playlists</h2>
        <PlaylistsGallery playlists={ user.playlists } />
      </div>
      <hr/>
      <div className="">
        <h2>Mes Albums</h2>
        <AlbumsGallery albums={ user.albums } />
      </div>
      <hr/>
      <div className="">
        <h2>Mes Artistes</h2>
        <ArtistsGallery artists={ user.artists } />
      </div>

    </div>
  }
}
