import React, { Component } from "react";
import { Button, Col, Image, Row, Table } from "react-bootstrap";
import { connect } from "react-redux";
import { updatePlayerList } from "../actions/player.action";
import { updateUser } from "../actions/user.action";
import Song from "../components/Song";
import PlaylistService from "../services/playlist.service";
import UserService from "../services/user.service";

class PlaylistDetails extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
      playlist: null
    };
  }

  async componentDidMount() {
    const { match: { params } } = this.props;
    try {
      let response = await PlaylistService.details(params.id);
      this.setState({ playlist: response.data.playlist });
    } catch (e) {
      console.error(e);
    }
  }

  async addLoved() {
    const { match: { params } } = this.props;
    if (this.state.loved) {
      try {
        let response = await UserService.unlinkPlaylist(params.id);
        response.status === 200 && this.setState({ loved: false });
      } catch (e) {
        console.error(e);
      }
    } else {
      try {
        let response = await UserService.addPlaylist(params.id);
        response.status === 200 && this.setState({ loved: true });
      } catch (e) {
        console.error(e);
      }
    }
  }

  async delete() {
    const { match: { params } } = this.props;
    try {
      let response = await PlaylistService.delete(params.id);
      response.status === 200 && this.props.history.push("/");
    } catch (e) {
      console.error(e);
    }
  }

  playPlaylist() {
    this.props.updatePlayerList(this.state.playlist.songs);
  }

  render() {
    let { playlist, loved } = this.state;

    return playlist && <div>
      <Row className="mb-3">
        <Col md="auto">
          <Image src="../no_cover.jpg" roundedCircle fluid />
        </Col>
        <Col>
          <h1>{ !playlist.loved_playlist ? playlist.name : "Coups de cœur" }</h1>
          {
            !playlist.loved_playlist && <Button variant="light text-danger" className="mr-2" onClick={ () => this.addLoved() }>
              { loved ? <i className="fas fa-heart"></i> : <i className="far fa-heart"></i> }
            </Button>
          }
          {
            playlist.songs.length > 0 && <Button variant="light" className={ this.props.user.role === 10 && "mr-2" } onClick={ () => this.playPlaylist() }>
              <i className="fas fa-play mr-2 text-success"></i>Lire la playlist
            </Button>
          }
          {
            this.props.user.role === 10 && !playlist.loved_playlist && <Button variant="danger" onClick={ () => this.delete() }>
              <i className="fas fa-trash"></i>
            </Button>
          }
        </Col>
      </Row>

      <Table className="mt-3">
        <thead>
          <tr className="font-weight-bold">
            <td></td>
            <td>Titre</td>
            <td>Artistes</td>
            <td>Année</td>
          </tr>
        </thead>
        <tbody>
          {
            playlist.songs && playlist.songs.map((song, index) => {
              return <Song
                key={ index }
                id={ song._id }
                name={ song.title }
                artists={ song.artists }
                year={ song.year } />
            })
          }
        </tbody>
      </Table>
    </div>
  }

}

const mapStateToProps = state => {
  return {
    user: state.user,
    playerList : state.playerList
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateUser: user => dispatch(updateUser(user)),
    updatePlayerList: playerList => dispatch(updatePlayerList(playerList))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PlaylistDetails);
