import React, { Component } from "react";
import UserService from "../services/user.service";
import PlaylistService from "../services/playlist.service";
import PlaylistsGallery from "../components/Gallery/PlaylistsGallery";
import {Row} from "react-bootstrap";
import Col from 'react-bootstrap/Col'


export default class Profile extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
      user: null,
      creator: null,
      name: null
    };
  }

  async componentDidMount() {
    try {
      let response = await UserService.details();
      this.setState({ user: response.data.user });
    } catch (e) {
      console.error(e);
    }
  }

  handleChange(e) {
    this.setState({ [e.target.id]: e.target.value });
  }

  async submit(e) {
    e.preventDefault();
    let { name } = this.state;
    let creator = JSON.parse(localStorage.getItem("user"))._id;
    let body = { name, creator };
    await PlaylistService.create(body);
  }

  render() {
    let { user } = this.state;

    return user && <Row>
      <Col className="mb-4" xs={"12"}>
        <h1>Profile de { user.name }</h1>
        <div>
          <div>
            Nom : { user.name }
          </div>
          <div>
            Email : { user.email }
          </div>
          <div>
            Role : {user.role = 10 ? "admin" : "utilisateur"}
          </div>
        </div>
      </Col>
      <Col xs="12">
        <Row>
        <Col xs="12" lg="6">
          <h2>Ajouter une playlist</h2>
          <form onSubmit={ (e) => this.submit(e) }>
            <div className="form-group">
              <label>
                <div>
                  Nom de ma playlist
                </div>
              </label>
              <input
                  id="name"
                  type="text"
                  name="name"
                  onChange={ (e) => this.handleChange(e) }
              />
            </div>
            <input type="submit" value="Creer une nouvelle playlist"/>
          </form>
        </Col>
        <Col xs="12" lg="6">
          <h2>Mes Playlists</h2>
          <PlaylistsGallery playlists={ user.playlists } />
        </Col>
        </Row>
      </Col>
    </Row>
  }

}
