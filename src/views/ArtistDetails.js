import React, { Component } from "react";
import { Image, Row, Col, Button, Accordion, Card } from "react-bootstrap";
import { connect } from "react-redux";
import { updateUser } from "../actions/user.action";
import AlbumsGallery from "../components/Gallery/AlbumsGallery";
import ArtistService from "../services/artist.service";
import UserService from "../services/user.service";

class ArtistDetails extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
      artist: null,
      loved: false
    };
  }

  async componentDidMount() {
    const { match: { params } } = this.props;
    try {
      let response = await ArtistService.details(params.id);
      this.setState({ artist: response.data.artist, loved: response.data.loved });
    } catch (e) {
      console.error(e);
    }
  }

  async addLoved() {
    const { match: { params } } = this.props;
    if (this.state.loved) {
      try {
        let response = await UserService.unlinkArtist(params.id);
        response.status === 200 && this.setState({ loved: false });
      } catch (e) {
        console.error(e);
      }
    } else {
      try {
        let response = await UserService.addArtist(params.id);
        response.status === 200 && this.setState({ loved: true });
      } catch (e) {
        console.error(e);
      }
    }
  }

  async delete() {
    const { match: { params } } = this.props;
    try {
      let response = await ArtistService.delete(params.id);
      response.status === 200 && this.props.history.push("/");
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    let { artist, loved } = this.state;

    return artist && <div>
      <Row className="mb-3">
        <Col md="auto">
          {
            artist.profile_photo ?
            <Image src={ process.env.REACT_APP_HOST_API + artist.profile_photo } roundedCircle fluid /> :
            <Image src="../no_cover.jpg" roundedCircle fluid />
          }
        </Col>
        <Col>
          <h1>{ artist.name }</h1>
          <Button variant="light text-danger" className={ this.props.user.role === 10 && "mr-2" } onClick={ () => this.addLoved() }>
            { loved ? <i className="fas fa-heart"></i> : <i className="far fa-heart"></i> }
          </Button>
          {
            this.props.user.role === 10 && <Button variant="danger" onClick={ () => this.delete() }>
              <i className="fas fa-trash"></i>
            </Button>
          }
          {
            artist.bio && <Accordion className="my-4">
              <Card bg="dark">
                <Accordion.Toggle as={ Card.Header } eventKey="0">
                  Bio
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="0">
                  <Card.Body>{ artist.bio }</Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
          }
        </Col>
      </Row>

      {
        artist.albums.length > 0 && <div>
          <h2>Albums</h2>
          <AlbumsGallery albums={ artist.albums } />
        </div>
      }
    </div>
  }

}
const mapStateToProps = state => {
  return {
    user: state.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateUser: user => dispatch(updateUser(user))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ArtistDetails);
