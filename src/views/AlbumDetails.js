import React, { Component } from "react";
import { Button, Col, Image, Row, Table } from "react-bootstrap";
import Song from "../components/Song";
import AlbumService from "../services/album.service";
import UserService from "../services/user.service";
import { updatePlayerList } from "../actions/player.action";
import { connect } from "react-redux";
import { updateUser } from "../actions/user.action";

class AlbumDetails extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
      album: null,
      loved: false
    };
  }

  async componentDidMount() {
    const { match: { params } } = this.props;
    try {
      let response = await AlbumService.details(params.id);
      this.setState({ album: response.data.album, loved: response.data.loved });
    } catch (e) {
      console.error(e);
    }
  }

  async addLoved() {
    const { match: { params } } = this.props;
    if (this.state.loved) {
      try {
        let response = await UserService.unlinkAlbum(params.id);
        response.status === 200 && this.setState({ loved: false });
      } catch (e) {
        console.error(e);
      }
    } else {
      try {
        let response = await UserService.addAlbum(params.id);
        response.status === 200 && this.setState({ loved: true });
      } catch (e) {
        console.error(e);
      }
    }
  }

  async delete() {
    const { match: { params } } = this.props;
    try {
      let response = await AlbumService.delete(params.id);
      response.status === 200 && this.props.history.push("/");
    } catch (e) {
      console.error(e);
    }
  }

  playAlbum() {
    this.props.updatePlayerList(this.state.album.songs);
  }

  render() {
    let { album, loved } = this.state;

    return album && <div>
      <Row>
        <Col md="auto">
          <Image src={ process.env.REACT_APP_HOST_API + album.cover } rounded fluid />
        </Col>
        <Col>
          <h1>{ album.name }</h1>
          <Button variant="light text-danger" className="mr-2" onClick={ () => this.addLoved() }>
            { loved ? <i className="fas fa-heart"></i> : <i className="far fa-heart"></i> }
          </Button>
          <Button variant="light" className={ this.props.user.role === 10 && "mr-2" } onClick={ () => this.playAlbum() }>
            <i className="fas fa-play mr-2 text-success"></i>Lire l'album
          </Button>
          {
            this.props.user.role === 10 && <Button variant="danger" onClick={ () => this.delete() }>
              <i className="fas fa-trash"></i>
            </Button>
          }
        </Col>
      </Row>
      <Table className="mt-3">
        <thead>
          <tr className="font-weight-bold">
            <td colSpan="2">#</td>
            <td>Titre</td>
            <td>Artistes</td>
            <td>Année</td>
          </tr>
        </thead>
        <tbody>
          {
            album.songs && album.songs.map((song, index) => {
              return <Song
                key={ index }
                id={ song._id }
                number={ song.track_num }
                name={ song.title }
                artists={ song.artists }
                year={ song.year } />
            })
          }
        </tbody>
      </Table>
    </div>
  }

}

const mapStateToProps = state => {
  return {
    user: state.user,
    playerList : state.playerList
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateUser: user => dispatch(updateUser(user)),
    updatePlayerList: playerList => dispatch(updatePlayerList(playerList))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AlbumDetails);
