import React, { Component } from "react";
import AjoutSons from "../components/Form/AjoutSon";
import AjoutGenre from "../components/Form/AjoutGenre";
import AjoutArtiste from "../components/Form/AjoutArtiste";
import AjoutAlbum from "../components/Form/AjoutAlbum";

export default class Admin extends Component {

  render() {
    return <div>
      <AjoutAlbum></AjoutAlbum>


      <AjoutSons></AjoutSons>
      <AjoutGenre></AjoutGenre>
      <AjoutArtiste></AjoutArtiste>
    </div>
  }

}
