const initState = {
  user: null,
  token: null,
  search: null,
  playerList: null
};

const rootReducer = (state = initState, action) => {

  switch (action.type) {
    case "UPDATE_TOKEN":
      return { ...state, token: action.token };
    case "UPDATE_USER":
      return { ...state, user: action.user };
    case "UPDATE_SEARCH":
      return { ...state, search: action.search };
    case "UPDATE_PLAYER_LIST":
      return { ...state, playerList: action.playerList };
    default:
      return state;
  };

};

export default rootReducer;
