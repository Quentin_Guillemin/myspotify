import axios from "axios";

export default class ArtistService {

  static async list() {
    return await axios.get(`${process.env.REACT_APP_HOST_API}/artists`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async create(body) {
    return await axios.post(`${process.env.REACT_APP_HOST_API}/artists`, body, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        'Content-Type': 'multipart/form-data'
      }
    });
  }

  static async details(id) {
    return await axios.get(`${process.env.REACT_APP_HOST_API}/artists/${id}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async update(id, body) {
    return await axios.put(`${process.env.REACT_APP_HOST_API}/artists/${id}`, body, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async delete(id) {
    return await axios.delete(`${process.env.REACT_APP_HOST_API}/artists/${id}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async updateProfilePhoto(id, body) {
    return await axios.put(`${process.env.REACT_APP_HOST_API}/albums/${id}/profile_photo`, body, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        'Content-Type': 'multipart/form-data'
      }
    });
  }

  static async addAlbum(id, albumId) {
    return await axios.put(`${process.env.REACT_APP_HOST_API}/artists/${id}/albums/${albumId}`, null, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async unlinkAlbum(id, albumId) {
    return await axios.delete(`${process.env.REACT_APP_HOST_API}/artists/${id}/albums/${albumId}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

}
