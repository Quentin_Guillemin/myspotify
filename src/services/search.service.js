import axios from "axios";

export default class SearchService {

  static async search(search) {
    return await axios.get(`${process.env.REACT_APP_HOST_API}/search/${search}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

}
