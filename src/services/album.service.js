import axios from "axios";

export default class AlbumService {

  static async list() {
    return await axios.get(`${process.env.REACT_APP_HOST_API}/albums`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async create(body) {
    return await axios.post(`${process.env.REACT_APP_HOST_API}/albums`, body, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        'Content-Type': 'multipart/form-data'
      }
    });
  }

  static async details(id) {
    return await axios.get(`${process.env.REACT_APP_HOST_API}/albums/${id}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async update(id, body) {
    return await axios.put(`${process.env.REACT_APP_HOST_API}/albums/${id}`, body, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async delete(id) {
    return await axios.delete(`${process.env.REACT_APP_HOST_API}/albums/${id}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async updateCover(id, body) {
    return await axios.put(`${process.env.REACT_APP_HOST_API}/albums/${id}/cover`, body, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        'Content-Type': 'multipart/form-data'
      }
    });
  }

  static async addSong(id, songId) {
    return await axios.put(`${process.env.REACT_APP_HOST_API}/albums/${id}/songs/${songId}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async unlinkSong(id, songId) {
    return await axios.delete(`${process.env.REACT_APP_HOST_API}/albums/${id}/songs/${songId}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

}
