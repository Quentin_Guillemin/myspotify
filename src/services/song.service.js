import axios from "axios";

export default class SongService {

  static async isLoved(id) {
    return await axios.get(`${process.env.REACT_APP_HOST_API}/songs/${id}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async create() {
    return await axios.post(`${process.env.REACT_APP_HOST_API}/songs`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        'Content-Type': 'multipart/form-data'
      }
    });
  }

  static async update(id, body) {
    return await axios.put(`${process.env.REACT_APP_HOST_API}/users/${id}`, body, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async delete(id) {
    return await axios.delete(`${process.env.REACT_APP_HOST_API}/songs/${id}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async updateFile(id, body) {
    return await axios.put(`${process.env.REACT_APP_HOST_API}/songs/${id}/file`, body, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        'Content-Type': 'multipart/form-data'
      }
    });
  }

}
