import axios from "axios";

export default class PlaylistService {

  static async list() {
    return await axios.get(`${process.env.REACT_APP_HOST_API}/playlists`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async create(body) {
    return await axios.post(`${process.env.REACT_APP_HOST_API}/playlists`, body, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async details(id) {
    return await axios.get(`${process.env.REACT_APP_HOST_API}/playlists/${id}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async update(id, body) {
    return await axios.put(`${process.env.REACT_APP_HOST_API}/playlists/${id}`, body, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async delete(id) {
    return await axios.delete(`${process.env.REACT_APP_HOST_API}/playlists/${id}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async addSong(id, songId) {
    return await axios.put(`${process.env.REACT_APP_HOST_API}/playlists/${id}/songs/${songId}`, null, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async unlinkSong(id, songId) {
    return await axios.delete(`${process.env.REACT_APP_HOST_API}/playlists/${id}/songs/${songId}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

}
