import axios from "axios";

export default class UserService {

  static async list() {
    return await axios.get(`${process.env.REACT_APP_HOST_API}/users`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async create(body) {
    return await axios.post(`${process.env.REACT_APP_HOST_API}/users`, body);
  }

  static async details() {
    return await axios.get(`${process.env.REACT_APP_HOST_API}/users/${JSON.parse(localStorage.getItem("user"))._id}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async update(body) {
    return await axios.put(`${process.env.REACT_APP_HOST_API}/users/${JSON.parse(localStorage.getItem("user"))._id}`, body, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async delete() {
    return await axios.delete(`${process.env.REACT_APP_HOST_API}/users/${JSON.parse(localStorage.getItem("user"))._id}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async addPlaylist(playlistId) {
    return await axios.put(`${process.env.REACT_APP_HOST_API}/users/${JSON.parse(localStorage.getItem("user"))._id}/playlists/${playlistId}`, null, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async unlinkPlaylist(playlistId) {
    return await axios.delete(`${process.env.REACT_APP_HOST_API}/users/${JSON.parse(localStorage.getItem("user"))._id}/playlists/${playlistId}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async addAlbum(albumId) {
    return await axios.put(`${process.env.REACT_APP_HOST_API}/users/${JSON.parse(localStorage.getItem("user"))._id}/albums/${albumId}`, null, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async unlinkAlbum(albumId) {
    return await axios.delete(`${process.env.REACT_APP_HOST_API}/users/${JSON.parse(localStorage.getItem("user"))._id}/albums/${albumId}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async addArtist(artistId) {
    return await axios.put(`${process.env.REACT_APP_HOST_API}/users/${JSON.parse(localStorage.getItem("user"))._id}/artists/${artistId}`, null, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async unlinkArtist(artistId) {
    return await axios.delete(`${process.env.REACT_APP_HOST_API}/users/${JSON.parse(localStorage.getItem("user"))._id}/artists/${artistId}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async addSong(songId) {
    return await axios.put(`${process.env.REACT_APP_HOST_API}/users/${JSON.parse(localStorage.getItem("user"))._id}/loved/${songId}`, null, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async unlinkSong(songId) {
    return await axios.delete(`${process.env.REACT_APP_HOST_API}/users/${JSON.parse(localStorage.getItem("user"))._id}/loved/${songId}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async auth(body) {
    return await axios.post(`${process.env.REACT_APP_HOST_API}/users/auth`, body);
  }

}
