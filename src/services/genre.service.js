import axios from "axios";

export default class GenreService {

  static async list() {
    return await axios.get(`${process.env.REACT_APP_HOST_API}/genres`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async create(body) {
    return await axios.post(`${process.env.REACT_APP_HOST_API}/genres`, body, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async details(id) {
    return await axios.get(`${process.env.REACT_APP_HOST_API}/genres/${id}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async update(id, body) {
    return await axios.put(`${process.env.REACT_APP_HOST_API}/genres/${id}`, body, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

  static async delete(id) {
    return await axios.delete(`${process.env.REACT_APP_HOST_API}/genres/${id}`, {
      headers: { 
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    });
  }

}
