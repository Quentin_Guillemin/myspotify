export const updateToken = (token) => {
  token ? localStorage.setItem("token", token) : localStorage.removeItem("token");

  return {
    type: "UPDATE_TOKEN",
    token: token
  }
};

export const updateUser = (user) => {
  user ? localStorage.setItem("user", JSON.stringify(user)) : localStorage.removeItem("user");

  return {
    type: "UPDATE_USER",
    user: user
  }
};
