export const updatePlayerList = (playerList) => {
    return {
        type: "UPDATE_PLAYER_LIST",
        playerList: playerList
    }
};
