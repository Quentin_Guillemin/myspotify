import React, { Component } from "react";
import artisteServices from "../../services/artist.service";
import Card from 'react-bootstrap/Card';

class AjoutArtiste extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: null,
            isError: false
        };
    }

    handleChange(e) {
        this.setState({ [e.target.id]: e.target.value });
    }

    async submit(e) {
        e.preventDefault();
        let { name, bio, profile_photo } = this.state;
        let body = { name, bio, profile_photo };
        artisteServices.create(body)
    }


    render() {
        return <Card bg="dark" style={{ width: '18rem' }}>
            <Card.Body>
                <Card.Title className="text-center"><h2>Ajouter un Artiste</h2></Card.Title>
                <form onSubmit={ (e) => this.submit(e) }>
                    <div>
                        <label>
                            Nom de l'artiste :
                        </label>
                        <input
                            id="name"
                            type="text"
                            name="name"
                            onChange={ (e) => this.handleChange(e) }
                        />
                    </div>
                    <div>
                        <label>
                            bio
                        </label>
                        <textarea
                            id="bio"
                            name="bio"
                            onChange={ (e) => this.handleChange(e) }
                        />
                    </div>
                    <div>
                        <label>
                            Photo de profile
                        </label>
                        <input
                            id="profile_photo"
                            type="file"
                            name="profile_photo"
                        />
                    </div>
                    <input type="submit" value="Ajouter un genre"/>
                </form>
            </Card.Body>
        </Card>
    }

}
export default AjoutArtiste
