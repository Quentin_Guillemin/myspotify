import React, { Component } from "react";
import genreServices from "../../services/genre.service";
import Card from 'react-bootstrap/Card';

class AjoutGenre extends Component {

    constructor(props) {
        super(props);

        this.state = {
            libelle: null,
            isError: false
        };
    }

    handleChange(e) {
        this.setState({ [e.target.id]: e.target.value });
    }

    async submit(e) {
        e.preventDefault();
        let { libelle } = this.state;
        let body = { libelle };
        genreServices.create(body)
    }


    render() {
        return <Card bg="dark" style={{ width: '18rem' }}>
            <Card.Body>
                <h2>Créer un genre</h2>
                <form onSubmit={ (e) => this.submit(e) }>
                    <div className="form-group">
                        <label>
                            Nom du genre
                        </label>
                        <input
                            id="libelle"
                            type="text"
                            name="libelle"
                            onChange={ (e) => this.handleChange(e) }
                        />
                    </div>
                    <input type="submit" value="Ajouter un genre"/>
                </form>
            </Card.Body>
        </Card>
    }

}
export default AjoutGenre;
