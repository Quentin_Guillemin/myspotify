import React, { Component } from "react";
import albumServices from "../../services/album.service";
import Card from 'react-bootstrap/Card';

class AjoutAlbum extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: null,
            isError: false
        };
    }

    handleChange(e) {
        this.setState({ [e.target.id]: e.target.value });
    }

    async submit(e) {
        e.preventDefault();
        let { name, years, cover, description } = this.state;
        let body = { name, years, cover, description };
        albumServices.create(body)
    }


    render() {
        return <Card bg="dark" style={{ width: '18rem' }}>
            <Card.Body>
                <Card.Title className="text-center"><h2>Ajouter un album</h2></Card.Title>
                    <form onSubmit={ (e) => this.submit(e) }>
                        <div className="form-group">
                            <label>
                                Nom de l'album :
                            </label>
                            <input
                                id="name"
                                type="text"
                                name="name"
                                onChange={ (e) => this.handleChange(e) }
                            />
                        </div>
                        <div className="form-group">
                            <label>
                                Date de création de l album :
                            </label>
                            <input
                                id="years"
                                type="date"
                                name="years"
                                onChange={ (e) => this.handleChange(e) }
                            />
                        </div>
                        <div className="form-group">
                            <label>
                                Ajout d'une cover :
                            </label>
                            <input
                                id="cover"
                                type="file"
                                name="cover"
                                onChange={ (e) => this.handleChange(e) }
                            />
                        </div>
                        <div className="form-group">
                            <label>
                                Ajout d'une description :
                            </label>
                            <textarea
                                id="description"
                                name="description"
                                onChange={ (e) => this.handleChange(e) }
                            />
                        </div>
                        <input type="submit" value="Ajouter un Album"/>
                    </form>
                </Card.Body>
            </Card>
    }

}

export default AjoutAlbum
