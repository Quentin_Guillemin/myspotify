import React, { Component } from "react";
import SonsService from "../../services/song.service";
import Select from "react-select";
import GenreService from "../../services/genre.service";
import { Button, Card, Form } from "react-bootstrap";

export default class AjoutSons extends Component {

  constructor(props) {
    super(props);

    this.state = {
      name: null,
      date: null,
      file_location: null,
      genres: [],
      genresList: [],
      isError: false,
    };
  }

  async componentDidMount() {
    let response = await GenreService.list();
    this.setState({ genresList: response.data.genres });
  }

  handleChange(e) {
    this.setState({ [e.target.id]: e.target.value });
  }

  handleChangeGenre(e) {
    let genres = e.map(genre => genre.value);
    this.setState({ genres });
  }

  async submit(e) {
    e.preventDefault();
    let { name, date, file_location, genres } = this.state;
    let body = { name, date, file_location, genres };
    try {
      let response = await SonsService.create(body);
    } catch (e) {
      this.setState({ isError: true });
    }
  }

  render() {
    let { genresList, isError } = this.state;

    return genresList && <div>
      <Card bg="dark" style={{ width: '18rem' }}>
        <Card.Body>
          <Card.Title className="text-center"><h2>Ajouter un son</h2></Card.Title>
          <Form onSubmit={ (e) => this.submit(e) }>

            <div className="form-group">
              <label>Titre</label>
              <input type="text" required className="form-control" id="name"
                onChange={ (e) => this.handleChange(e) }/>
            </div>

            <div className="form-group">
              <label>Date</label>
              <input type="date" required className="form-control" id="date"
                onChange={ (e) => this.handleChange(e) }/>
            </div>

            <div className="form-group">
              <label>Audio</label>
              <input type="file" required className="form-control" id="file_location"
                onChange={ (e) => this.handleChange(e) }/>
            </div>

            <div className="form-group">
              <label>Genres</label>
              <Select
                isMulti={ true }
                options={ genresList.map(genre => { return { value: genre._id , label: genre.libelle } }) }
                onChange={ (e) => this.handleChangeGenre(e) } />
            </div>

          <Button type="submit" className="primary w-100">Ajouter un son</Button>

          { isError && <div className="pt-3">Impossible de créer le son</div> }

        </Form>
        </Card.Body>
      </Card>
    </div>
  }

}
