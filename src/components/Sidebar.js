import React, { Component } from "react";
import { Button, Form, FormControl, InputGroup, Nav, Navbar } from "react-bootstrap";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { updateToken, updateUser } from "../actions/user.action";
import { updateSearch } from "../actions/search.action";
import SearchService from "../services/search.service";

class Sidebar extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
      searchValue: ""
    };
  }

  logout() {
    this.props.updateToken(null);
    this.props.updateUser(null);
  }

  async searchAction(e) {
    let searchValue = e.target.value;
    this.setState({ searchValue });
    if (searchValue.length > 0) {
      try {
        let response = await SearchService.search(searchValue);
        this.props.updateSearch(response.data);
      } catch (e) {
        console.error(e);
        this.props.updateSearch(null);
      }
    } else this.props.updateSearch(null);
  }

  eraseSearch() {
    this.setState({ searchValue: "" });
    this.props.updateSearch(null);
  }

  render() {
    return <Navbar bg="dark" expand="sm" fixed="top">
      <Navbar.Brand><b><Link to={ "/" }>My Spotify</Link></b></Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Link className="nav-link text-light" to={ "/profile" }>Profile</Link>
          <Link className="nav-link text-light" to={ "/genres" }>Genres</Link>
          {
            this.props.user.role === 10 && <Link className="nav-link text-light" to={ "/admin" }>Admin</Link>
          }
        </Nav>
        <Form inline className="mr-md-2">
          <InputGroup>
            <FormControl type="text" placeholder="Recherche" onChange={ (e) => this.searchAction(e) } value={ this.state.searchValue } />
            <InputGroup.Append>
              <Button className="bg-dark" onClick={ () => this.eraseSearch() }><i className="fa fa-times"></i></Button>
            </InputGroup.Append>
          </InputGroup>
        </Form>
        <Nav>
          <Link className="nav-link text-danger" to={ "/" } onClick={ () => this.logout() }><i className="fas fa-sign-out-alt"></i></Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  }

}

const mapStateToProps = state => {
  return {
    user: state.user,
    userToken: state.userToken,
    search: state.search
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateUser: user => dispatch(updateUser(user)),
    updateToken: token => dispatch(updateToken(token)),
    updateSearch: search => dispatch(updateSearch(search))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
