import React, { Component } from "react";
import { Row, Col } from "react-bootstrap";
import ReactAudioPlayer from 'react-audio-player';
import {connect} from "react-redux";
import { updatePlayerList } from "../actions/player.action";

class Player extends Component{



    constructor(props) {
        super(props);

        this.state = {
            ToggleOnlike:true,
            numero: 0,
            tableSons: null,
        };
        this.clickjaime = this.clickjaime.bind(this);
    }

    componentDidMount() {
        let tableSons = this.props.playerList;
        this.setState({tableSons})
    }


    handleChangelike(e) {
        this.setState({ [e.target.id]: e.target.value });
    }

    clickjaime() {
        this.setState(prevState => ({
            isToggleOnlike: !prevState.isToggleOnlike
        }));
    }

    next(){
        let numero = this.state.numero === this.props.playerList.length - 1 ?  this.state.numero = this.props.playerList.length -1 : this.state.numero +1;
        this.setState({
            numero
        });
    }

    previous(){
        let numero = this.state.numero === 0 ? 0 : this.state.numero - 1;
        this.setState({
            numero
        });
    }

    render() {
        let tableSons = this.props.playerList;
        let numero = this.state.numero;
        console.log(tableSons);

        return <div className="player">
            <Row>
                <Col xs={4}>
                    <Row>
                        <Col md="auto" >
                            <div>{tableSons ?  tableSons[numero].title : ""}</div>
                            <div>{tableSons ?  tableSons[numero].artists[0].name : ""}</div>
                        </Col>
                    </Row>
                </Col>
                <Col xs={8}>
                    <Row className="justify-content-md-center">
                        <Col>
                            <i onClick={()=>this.previous()} className="fas fa-step-backward"></i>
                            <ReactAudioPlayer
                                src={tableSons ? process.env.REACT_APP_HOST_API + tableSons[numero].file_location : ""}
                                autoPlay
                                controls
                                onEnded={()=>this.next()}
                            />
                            <i onClick={()=>this.next()} className="fas fa-step-forward"></i>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </div>
    }
}

const mapStateToProps = state => {
    return {
        playerList : state.playerList,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        updatePlayerList: playerList => dispatch(updatePlayerList(playerList)),
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Player);
