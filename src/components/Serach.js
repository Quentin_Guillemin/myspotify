import React, { Component } from "react";
import { Table } from "react-bootstrap";
import AlbumsGallery from "./Gallery/AlbumsGallery";
import ArtistsGallery from "./Gallery/ArtistsGallery";
import PlaylistsGallery from "./Gallery/PlaylistsGallery";
import Song from "./Song";

export default class Search extends Component {

  render() {
    let { data } = this.props;

    return data && <div>
      {
        data.albums.length > 0 && <div>
          <h2>Albums</h2>
          <AlbumsGallery albums={ data.albums } />
        </div>
      }
      {
        data.artists.length > 0 && <div>
          <h2>Artistes</h2>
          <ArtistsGallery artists={ data.artists } />
        </div>
      }
      {
        data.playlists.length > 0 && <div>
          <h2>Playlists</h2>
          <PlaylistsGallery playlists={ data.playlists } />
        </div>
      }
      {
        data.songs.length > 0 && <div>
          <h2>Titres</h2>
          <Table>
            <thead>
              <tr className="font-weight-bold">
                <td colSpan="2">Titre</td>
                <td>Artistes</td>
                <td>Année</td>
              </tr>
            </thead>
            <tbody>
              {
                data.songs.map((song, index) => {
                  return <Song
                    key={ index }
                    id={ song._id }
                    name={ song.title }
                    artists={ song.artists }
                    year={ song.year } />
                })
              }
            </tbody>
          </Table>
        </div>
      }
    </div>
  }

}
