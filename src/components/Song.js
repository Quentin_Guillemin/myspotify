import React, { Component } from "react";
import moment from "moment";
import { Button } from "react-bootstrap";
import UserService from "../services/user.service";
import SongService from "../services/song.service";
import { Link } from "react-router-dom";
import { updateUser } from "../actions/user.action";
import { connect } from "react-redux";

class Song extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
      loved: false
    };
  }

  async componentDidMount() {
    try {
      let response = await SongService.isLoved(this.props.id);
      this.setState({ loved: response.data.loved });
    } catch (e) {
      console.error(e);
    }
  }

  async addLoved(id) {
    if (this.state.loved) {
      try {
        let response = await UserService.unlinkSong(id);
        response.status === 200 && this.setState({ loved: false });
      } catch (e) {
        console.error(e);
      }
    } else {
      try {
        let response = await UserService.addSong(id);
        response.status === 200 && this.setState({ loved: true });
      } catch (e) {
        console.error(e);
      }
    }
  }

  async delete(id) {
    try {
      let response = await SongService.delete(id);
      response.status === 200 && this.props.history.push("/");
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    let { id, number, name, year, artists } = this.props;
    let { loved } = this.state;

    return <tr>
      {
        number && <td>{ number }</td>
      }
      <td>
        <Button variant="transparent text-danger" onClick={ () => this.addLoved(id) }>
          { loved ? <i className="fas fa-heart"></i> : <i className="far fa-heart"></i> }
        </Button>
        {
          this.props.user.role === 10 && <Button variant="transparent text-danger" onClick={ () => this.delete(id) }>
            <i className="fas fa-trash"></i>
          </Button>
        }
      </td>
      <td>{ name }</td>
      <td>{ artists && artists.map((artist, index) => {
        return <div key={ index } className="d-inline mr-2">
            <Link to={ `/artists/${artist._id}` }>{ artist.name }</Link>
          </div>
      }) }</td>
      <td>{ moment(year).format("M-Y") }</td>
    </tr>
  }

}

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateUser: user => dispatch(updateUser(user))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Song);