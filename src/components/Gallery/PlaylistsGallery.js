import React, { Component } from "react";
import { Col, Image, Row } from "react-bootstrap";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { updateSearch } from "../../actions/search.action";

class PlaylistsGallery extends Component {

  closeSearch() {
    this.props.updateSearch(null);
  }

  render() {
    let { playlists } = this.props;

    return <Row xs="1" sm="2" md="4" lg="6">
      {
        playlists.length > 0 && playlists.map((playlist, index) => {
          return <Col key={ index }>
            <Link to={ `/playlists/${playlist._id}` } onClick={ () => this.closeSearch() }>
              <Image src="no_cover.jpg" rounded fluid />
              <h3 className="text-center">{ !playlist.loved_playlist ? playlist.name : "Coups de cœur" }</h3>
            </Link>
          </Col>
        })
      }
    </Row>
  }

}

const mapStateToProps = state => {
  return {
    search: state.search
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateSearch: search => dispatch(updateSearch(search))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PlaylistsGallery);
