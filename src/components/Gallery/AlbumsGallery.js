import React, { Component } from "react";
import { Col, Image, Row } from "react-bootstrap";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { updateSearch } from "../../actions/search.action";

class AlbumsGallery extends Component {

  closeSearch() {
    this.props.updateSearch(null);
  }

  render() {
    let { albums } = this.props;

    return <Row xs="1" sm="2" md="4" lg="6">
      {
        albums.length > 0 && albums.map((album, index) => {
          return <Col key={ index }>
            <Link to={ `/albums/${album._id}` } onClick={ () => this.closeSearch() }>
              <Image src={ process.env.REACT_APP_HOST_API + album.cover } rounded fluid />
              <h3 className="text-center">{ album.name }</h3>
            </Link>
          </Col>
        })
      }
    </Row>
  }

}

const mapStateToProps = state => {
  return {
    search: state.search
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateSearch: search => dispatch(updateSearch(search))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AlbumsGallery);
