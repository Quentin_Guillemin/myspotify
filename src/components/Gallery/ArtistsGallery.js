import React, { Component } from "react";
import { Col, Image, Row } from "react-bootstrap";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { updateSearch } from "../../actions/search.action";

class ArtistsGallery extends Component {

  closeSearch() {
    this.props.updateSearch(null);
  }

  render() {
    let { artists } = this.props;

    return <Row xs="1" sm="2" md="4" lg="6">
      {
        artists.length > 0 && artists.map((artist, index) => {
          return <Col key={ index }>
            <Link to={ `/artists/${artist._id}` } onClick={ () => this.closeSearch() }>
              {
                artist.profile_photo ?
                <Image src={ process.env.REACT_APP_HOST_API + artist.profile_photo } roundedCircle fluid /> :
                <Image src="no_cover.jpg" roundedCircle fluid />
              }
              <h3 className="text-center">{ artist.name }</h3>
            </Link>
          </Col>
        })
      }
    </Row>
  }

}

const mapStateToProps = state => {
  return {
    search: state.search
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateSearch: search => dispatch(updateSearch(search))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ArtistsGallery);
