import React, { Component } from "react";
import { Card, Col, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

export default class GenresGallery extends Component {

  render() {
    let { genres } = this.props;

    return <Row xs="1" sm="2" md="4" lg="6">
      {
        genres.length > 0 && genres.map((genre, index) => {
          return <Col key={ index }>
            <Link to={ `/genres/${genre._id}` }>
              <Card className="my-3" bg="transparent">
                <Card.Body>
                  <Card.Title className="text-center">{ genre.libelle }</Card.Title>
                </Card.Body>
              </Card>
            </Link>
          </Col>
        })
      }
    </Row>
  }

}
