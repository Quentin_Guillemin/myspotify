import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import { connect } from "react-redux";
import { updateUser, updateToken } from "./actions/user.action";
import { updateSearch } from "./actions/search.action";
import { Container } from "react-bootstrap";
import Admin from "./views/Admin";
import AlbumDetails from "./views/AlbumDetails";
import ArtistDetails from "./views/ArtistDetails";
import GenreDetails from "./views/Genres/GenreDetails";
import GenresList from "./views/Genres/GenresList";
import Home from "./views/Home";
import Login from "./views/Login";
import PlaylistDetails from "./views/PlaylistDetails";
import Player from "./components/Player";
import Profile from "./views/Profile";
import Sidebar from "./components/Sidebar";
import Search from "./components/Serach";
import "./App.scss";

class App extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
      logged: false
    };
  }

  async componentDidMount() {
    let user = localStorage.getItem("user");
    let token = localStorage.getItem("token");
    if (user && token) {
      await this.props.updateUser(JSON.parse(user));
      await this.props.updateToken(token);
    }
  }

  render() {
    return this.props.user && this.props.token ? <BrowserRouter>

      <Sidebar />

      <Container className="content" fluid>
        <div className="px-2 py-3">
          {
            this.props.search ? <Search data={ this.props.search } /> :
            <div>
              <Route exact path="/" component={ Home } />

              {/* GENRES */}
              <Route exact path="/genres" component={ GenresList } />
              <Route exact path="/genres/:id" component={ GenreDetails } />

              {/* PLAYLISTS */}
              <Route exact path="/playlists/:id" component={ PlaylistDetails } />

              {/* ALBUMS */}
              <Route exact path="/albums/:id" component={ AlbumDetails } />

              {/* ARTISTS */}
              <Route exact path="/artists/:id" component={ ArtistDetails } />

              {/* USER */}
              <Route exact path="/profile" component={ Profile } />

              {/* ADMIN */}
              {
                this.props.user.role === 10 && <Route exact path="/admin" component={ Admin } />
              }
              
            </div>
          }
        </div>
      </Container>

      <Player />

    </BrowserRouter> : <Container fluid>
      <Login />
    </Container>
  }

}

const mapStateToProps = state => {
  return {
    user: state.user,
    token: state.token,
    search: state.search
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateUser: user => dispatch(updateUser(user)),
    updateToken: token => dispatch(updateToken(token)),
    updateSearch: search => dispatch(updateSearch(search))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
